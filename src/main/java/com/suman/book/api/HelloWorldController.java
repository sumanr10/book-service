package com.suman.book.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@GetMapping(value = "/namaste")
	public String sayHello() {
		return "Welcome to Rest Web Services";
	}
}
