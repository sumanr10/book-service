package com.suman.book.api;

import java.net.http.HttpHeaders;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.suman.book.model.BookModel;
import com.suman.book.model.BookSearch;
import com.suman.book.model.HeaderInfo;
import com.suman.book.service.BookService;

@RestController
public class BookApi {

	@Autowired
	private BookService bookService;

	@GetMapping(value = "/book")
	public ResponseEntity<String> healthCheck() {
		return ResponseEntity.ok("BookService is healthy");
	}

	@GetMapping(value = "/book/{bookId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BookModel> findbyId(@PathVariable("bookId") Long bookId) {

		final BookModel model = bookService.find(bookId);
		return ResponseEntity.ok(model);
	}

	@PostMapping(value = "/book", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BookModel> create(@RequestBody BookModel bookModel) {

		final BookModel model = bookService.create(bookModel);
		return ResponseEntity.ok(model);
	}

	@PutMapping(value = "/book", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BookModel> modify(@RequestBody BookModel bookModel) {

		bookService.modify(bookModel);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping(value = "/book/{bookId}")
	public ResponseEntity<BookModel> remove(@PathVariable("bookId") Long bookId) {

		bookService.remove(bookId);
		return ResponseEntity.noContent().build();
	}

	@GetMapping(value = "/book/search", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BookSearch> search(@RequestParam("bookName") String bookName,
			@RequestParam(value = "bookAuthor", required = false) String bookAuthor) {

		BookSearch response = bookService.search(bookName, bookAuthor);
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/book/search1", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BookSearch> search1(@RequestParam Map<String, String> rMap) {

		BookSearch response = bookService.search(rMap.get("bookName"), rMap.get("bookAuthor"));
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "book/hdr", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<HeaderInfo> header(@RequestHeader Map<String, String> header) {
		final HeaderInfo result = HeaderInfo.builder().appName(header.get("app-name"))
				.authToken(header.get("auth-token")).appVersion(header.get("app-version")).build();
		return ResponseEntity.ok(result);
	}

}
