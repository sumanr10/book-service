package com.suman.book.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.suman.book.exception.InvalidDataException;
import com.suman.book.exception.RecordNotFoundException;
import com.suman.book.model.ErrorInfo;

@ControllerAdvice
public class ExceptionController {
	@ExceptionHandler(RecordNotFoundException.class)
	public ResponseEntity<ErrorInfo> recordNotFound(RecordNotFoundException ex) {
		final ErrorInfo info = ErrorInfo.builder().code("ERR-001").message(ex.getMessage()).build();
		return new ResponseEntity<ErrorInfo>(info, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(InvalidDataException.class)
	public ResponseEntity<ErrorInfo> incalidData(InvalidDataException ex) {
		final ErrorInfo info = ErrorInfo.builder().code("ERR-002").message(ex.getMessage()).build();
		return new ResponseEntity<ErrorInfo>(info, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorInfo> genericException(Exception ex) {
		final ErrorInfo info = ErrorInfo.builder().code("ERR-000").message("ERROR OCCURED ON THE SERVER SIDE").build();
		return new ResponseEntity<ErrorInfo>(info, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}