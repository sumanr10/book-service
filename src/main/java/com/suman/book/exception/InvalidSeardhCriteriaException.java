package com.suman.book.exception;

public class InvalidSeardhCriteriaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidSeardhCriteriaException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidSeardhCriteriaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidSeardhCriteriaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidSeardhCriteriaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidSeardhCriteriaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
