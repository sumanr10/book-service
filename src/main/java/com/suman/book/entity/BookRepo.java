package com.suman.book.entity;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepo extends CrudRepository<BookEntity, Long> {

	@Query("select b from BookEntity b where b.bookName like : bookName and b.bookAuthor like : bookAuthor")
	List<BookEntity> search(String bookName, String bookAuthor);

}
