package com.suman.book.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.mapping.Array;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookSearch {

	@Builder.Default
	private List<BookModel> books = new ArrayList<>();
}
