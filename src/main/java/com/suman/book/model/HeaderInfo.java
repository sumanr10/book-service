package com.suman.book.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor

public class HeaderInfo {

	private String authToken;
	private String appName;
	private String appVersion;
	private String contentType;

}
