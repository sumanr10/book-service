package com.suman.book.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor

public class BookModel {
	
	private Long bookId;
	private String bookName;
	private String bookAuthor;
	private Integer publishedYear;
	

}
