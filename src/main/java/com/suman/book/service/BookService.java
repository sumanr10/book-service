package com.suman.book.service;

import com.suman.book.model.BookModel;
import com.suman.book.model.BookSearch;

public interface BookService {

	BookModel find(final Long bookId);

	BookModel create(final BookModel bookModel);

	void modify(final BookModel bookModel);

	void remove(final Long id);

	BookSearch search(String bookName, String bookAuthor);
}
