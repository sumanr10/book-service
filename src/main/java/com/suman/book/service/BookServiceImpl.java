package com.suman.book.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.suman.book.entity.BookEntity;
import com.suman.book.entity.BookRepo;
import com.suman.book.exception.InvalidDataException;
import com.suman.book.exception.RecordNotFoundException;
import com.suman.book.model.BookModel;
import com.suman.book.model.BookSearch;

@Service
@Transactional
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepo bookRepo;

	@Override
	public BookModel find(Long bookId) {

		Optional<BookEntity> book = bookRepo.findById(bookId);

		if (!book.isPresent()) {
			throw new RecordNotFoundException("No book found for id=" + bookId);
		}
		return BookMapper.mapTo(book.get());

	}

	@Override
	public BookModel create(BookModel bookModel) {
		validate(bookModel);
		final BookEntity entity = BookMapper.mapTo(new BookEntity(), bookModel);
		bookRepo.save(entity);
		bookModel.setBookId(entity.getBookId());
		return bookModel;
	}

	@Override
	public void modify(BookModel bookModel) {
		validate(bookModel);
		final Optional<BookEntity> result = bookRepo.findById(bookModel.getBookId());
		if (!result.isPresent()) {
			throw new RecordNotFoundException("No book found to remove id with =" + bookModel.getBookId());
		}

		BookEntity entity = BookMapper.mapTo(result.get(), bookModel);
		bookRepo.save(entity);

	}

	@Override
	public void remove(Long bookId) {
		if (bookRepo.existsById(bookId)) {

			bookRepo.deleteById(bookId);
		} else {
			throw new RecordNotFoundException("No book found to remove id with =" + bookId);
		}
	}

	@Override
	public BookSearch search(String bookName, String bookAuthor) {
		BookSearch result = new BookSearch();
		bookName = (bookName == null) ? "%" : bookName + "%";
		bookAuthor = (bookAuthor == null) ? "%" : bookAuthor + "%";
		final List<BookEntity> results = bookRepo.search(bookName, bookAuthor);
		results.forEach(entity -> {
			result.getBooks().add(BookMapper.mapTo(entity));
		});
		return null;
	}

	private void validate(final BookModel model) {
		if (StringUtils.isEmpty(model.getBookName()) || StringUtils.isEmpty(model.getBookAuthor())) {
			throw new InvalidDataException("Invalid data to create or modify");
		}

	}

}
