package com.suman.book.service;

import com.suman.book.entity.BookEntity;
import com.suman.book.model.BookModel;

public class BookMapper {

	private BookMapper() {

	}

	public static BookModel mapTo(final BookEntity entity) {
		return BookModel.builder().bookId(entity.getBookId()).bookName(entity.getBookName())
				.bookAuthor(entity.getBookAuthor()).publishedYear(entity.getPublishedYear()).build();

	}

	public static BookEntity mapTo(final BookEntity entity, final BookModel model) {
		return BookEntity.builder().bookId(model.getBookId()).bookName(model.getBookName())
				.bookAuthor(model.getBookAuthor()).publishedYear(model.getPublishedYear()).build();

	}
}
